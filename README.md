Converts passed string into an array of mnemonic words

Static method called :

$output=\actweb\Mnemonic::convert($string);

Where $string='hEllow wOrld!'

Output would be :

Hotel

Uppercase Echo

Lima

Lima

Oscar

Whisky

Space

Whisky

Uppercase Oscar

Romeo

Lima

Delta

Exclamation Mark


Script can also be called from command line, and takes a single parameter as $string

When called from CLI, script just outputs the mnemonic to STDOUT


New static class added that returns a password from Dinopass.com

can be called with 'strong' or 'simple' to get a strong password or a simple one
the default is a strong password.

Static method : \actweb\Mnemonic::genPass([<strong>|<simple>])

Makes an API call to 'dinopass.com' and returns a password.

Please see the website http://dinopass.com for the definition of strong and simple passwords.

PLEASE NOTE : I have been unable to contact Kevan Stannard in the past to request his permission to use his API within this class, if anyone can indicate that he is not happy about me usng his API within this class, please let me know and i will remove the code.
