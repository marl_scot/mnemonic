<?php
/**
 * Mnemonic -> simple string converter
 *
 * This will take a string and return the mnomonic representation of
 * it.  It also has a method to generate a password via the API at
 * https://dinopass.com
 *
 * User: marl_scot
 * Date: 24/05/2018
 * Time: 19:00
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  ACTweb/utils
 * @package   phonetic
 * @author    MarlScot <marl.scot.1@googlemail.com>
 * @copyright 2018 MarlScot
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   1.2.1
 * @link      https://www.actweb.co.uk/package/mnomonic
 */


namespace actweb;

if ((PHP_SAPI === 'cli') && isset($argv) && (count($argv) > 1)) {
    $output = Mnemonic::convert($argv[1]);
    foreach ($output as $value) {
        echo $value . "\n";
    }
}


class Mnemonic
{
    /**
     * Character to Mnemonic array
     *
     * @var array
     */
    private static $mnemonic
        = array(
            'a' => 'Alpha',
            'b' => 'Bravo',
            'c' => 'Charlie',
            'd' => 'Delta',
            'e' => 'Echo',
            'f' => 'Foxtrot',
            'g' => 'Golf',
            'h' => 'Hotel',
            'i' => 'India',
            'j' => 'Juliet',
            'k' => 'Kilo',
            'l' => 'Lima',
            'm' => 'Mike',
            'n' => 'November',
            'o' => 'Oscar',
            'p' => 'Papa',
            'q' => 'Quebec',
            'r' => 'Romeo',
            's' => 'Sierra',
            't' => 'Tango',
            'u' => 'Uniform',
            'v' => 'Victor',
            'w' => 'Whisky',
            'x' => 'X-Ray',
            'y' => 'Yankee',
            'z' => 'Zulu',
            ',' => 'Comma',
            '!' => 'Exclamation Mark',
            '£' => 'Pound',
            '#' => 'Hash',
            '$' => 'Dollar',
            "'" => 'Single Quote',
            '"' => 'Double Quote',
            '%' => 'Percent',
            '^' => 'Hat',
            '&' => 'Ampersand',
            '*' => 'Asterisk',
            '(' => 'Open Round Brackets',
            ')' => 'Close Round Brackets',
            ' ' => 'Space',
            '[' => 'Open Square Bracket',
            ']' => 'Close Square Bracket',
            '{' => 'Open Curly Bracket',
            '}' => 'Close Curly Bracket',
            '@' => 'At Symbol',
            ':' => 'Colon',
            ';' => 'Semi Colon',
            '~' => 'Tilde',
            '<' => 'Lower Than Symbol',
            '>' => 'Greater Than Symbol',
            '?' => 'Question Mark',
            '|' => 'Pipe Symbol',
            '\\' => 'Back Slash',
            '/' => 'Forward Slash',
            '.' => 'Full Stop',
            '+' => 'Plus Symbol',
            '-' => 'Minus Symbol',
            '`' => 'Back Tick',
            '0' => 'Number Zero',
            '1' => 'Number One',
            '2' => 'Number Two',
            '3' => 'Number Three',
            '4' => 'Number Four',
            '5' => 'Number Five',
            '6' => 'Number Six',
            '7' => 'Number Seven',
            '8' => 'Number Eight',
            '9' => 'Number Nine',
        );

    public static $passUrl = 'http://www.dinopass.com/password/';

    /**
     * Basic Constructor
     */
    public function __construct()
    {
        // does nothing just now
    }

    /**
     * Converts the passed string to mnemonic
     * and returns result as an array
     *
     * @param string $string String to convert
     * @param bool   $inc    If true, then return each phonetic as an array with its character as well as its description
     * @param bool   $debug  If true, then output debug info to standard out
     *
     * @return array Array of mnemonic
     */
    public static function convert($string, $inc = false, $debug = false)
    {
        $output = array();
        $split = self::str_split_unicode($string);
        foreach ($split as $char) {
            if ($debug) {
                echo 'Character :' . $char . ":\n";
            }
            if (strtolower($char) !== $char) {
                $extra = 'Uppercase ';
                $char = strtolower($char);
            } else {
                $extra = '';
            }
            if (array_key_exists($char, self::$mnemonic)) {
                if ($inc) {
                    $dat = array($char, $extra . self::$mnemonic[$char]);
                } else {
                    $dat = $extra . self::$mnemonic[$char];
                }
            } else {
                if ($inc) {
                    $dat = array($char, $extra . 'Unknown : ' . $char);
                } else {
                    $dat = $extra . 'Unknown : ' . $char;
                }
            }
            $output[] = $dat;
        }
        return $output;
    }

    /**
     * Gets a password from Dinopass.com
     * Options are 'strong' or 'simple'
     * Returns password or null if there was a problem
     *
     * @param string $type 'strong' or 'simple' for strong or simple passwords
     *
     * @return string Password
     */
    public static function genPass($type = 'strong')
    {
        if (($type !== 'strong') && ($type !== 'simple')) {
            return null;
        }
        $password = file_get_contents(self::$passUrl . $type);
        if ($password === false) {
            return null;
        }
        return $password;
    }

    /**
     * Splits a string that is UTF-8 encoded
     *
     * @param string $str String to split
     * @param int    $l   Length of chunks to split string into
     *
     * @return array
     */
    public static function str_split_unicode($str, $l = 0)
    {
        if ($l > 0) {
            $ret = array();
            $len = mb_strlen($str, 'UTF-8');
            for ($i = 0; $i < $len; $i += $l) {
                $ret[] = mb_substr($str, $i, $l, 'UTF-8');
            }
            return $ret;
        }
        return preg_split('//u', $str, -1, PREG_SPLIT_NO_EMPTY);
    }
}
